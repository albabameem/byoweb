<?php
function sign($n) {
    return ($n > 0) - ($n < 0);
}

function videoThumbnail($link){
	$link = str_replace(array('.mp4','.mov', '.heic'), '.jpg', $link);
	$explodeLink = explode('/upload/', $link);
	$newLink = "$explodeLink[0]/upload/c_scale,q_auto,w_600/$explodeLink[1]";
	return $newLink;
}

function videoLink($link){
	$link = str_replace('.mov', '.mp4', $link);
	$explodeLink = explode('/upload/', $link);
	$newLink = "$explodeLink[0]/upload/c_scale,e_preview,q_auto,w_600/$explodeLink[1]";
	return $newLink;
}

function imageThumbnail($link){
	$linkExt = substr($link, -3);
	if($linkExt == 'mp4' || $linkExt == 'mov'){
		$newLink = videoThumbnail($link);
	}else{
		$link = str_replace('.heic', '.jpg', $link);
		$explodeLink = explode('/upload/', $link);
		$newLink = "$explodeLink[0]/upload/c_scale,q_auto,w_450/$explodeLink[1]";
	}
	
	return $newLink;
}

function imgMani($link, $quality = 'low', $width = '250', $scale = true){
	$linkExplode = explode('/upload/', $link);
	return(urlToJpg("$linkExplode[0]/upload/w_$width,q_auto:$quality".($scale ? ",c_scale" : "")."/$linkExplode[1]"));;
}

function urlToJpg($link){
	return str_replace(array('.heic','.png', '.gif'), '.jpg', $link);
}


function kFormatter($num) {
	
    if((abs($num)) > 999 && (abs($num)) < 1000000) {
	    return sign($num)*(round(abs($num)/1000,1)) . 'k';
    }else if((abs($num)) > 999999 && (abs($num)) < 1000000000) {
	    return sign($num)*(round(abs($num)/1000000,1)) . 'm';
    }else if((abs($num)) > 999999999) {
	    return sign($num)*(round(abs($num)/1000000000,1)) . 'b';
    }else{
    	return sign($num)*abs($num);
    }
}