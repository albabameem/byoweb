<?php
include 'base.php';

$method = $_POST['method'];

if($method == 'repCount' && $_POST['jid'] > 0){
	$jid = $_POST['jid'];

	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => ENV_PROTOCOL."df.".ENV_DOMAIN."/api/v2/shoutprod96/_proc/pjourenycommentcountr5%28".$jid."%29?wrapper=resource",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
	    "accept: application/json",
	    "cache-control: no-cache",
	    "content-type: application/json",
	    "postman-token: 832f51d8-fef5-9112-f3f2-36e84ee688f9",
	    "x-dreamfactory-api-key: 36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88"
	  ),
	));

	$response = curl_exec($curl);
	$arr = json_decode($response,true);
	$err = curl_error($curl);

	curl_close($curl);

	echo $arr['resource'][0]['count'];
}

if($method == 'interests' && $_POST['memberid'] > 0){
	$memberid = $_POST['memberid'];

	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => ENV_PROTOCOL."df.".ENV_DOMAIN."/api/v2/shoutprod96/_table/vtvuserinterests_r3j1334?filter=memberid%3D${memberid}",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
	    "accept: application/json",
	    "cache-control: no-cache",
	    "content-type: application/json",
	    "postman-token: 832f51d8-fef5-9112-f3f2-36e84ee688f9",
	    "x-dreamfactory-api-key: 36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88"
	  ),
	));

	$response = curl_exec($curl);
	$arr = json_decode($response,true);
	$err = curl_error($curl);

	curl_close($curl);
	$output = '';
	foreach ($arr['resource'] as $interest) {
		$output .= '<img src="'.ENV_PROTOCOL.ENV_DOMAIN.'/UploadActIcon/'.$interest['iconURL'].'" class="img-fluid mr-3" style="max-height: 2.25rem;">';
	}

	echo $output;
}

if($method == 'grid' && $_POST['memberid'] > 0){
	$memberid = $_POST['memberid'];
	$offset = $_POST['offset'];
	$curl = curl_init();

	curl_setopt_array($curl, array(
	  //CURLOPT_URL => ENV_PROTOCOL.ENV_DOMAIN."/api/byocustomapi/profilegridjourney.php?memberid=${memberid}&journeyid=0&limit=10&offset=$offset", 	--old
	  CURLOPT_URL => ENV_PROTOCOL.ENV_DOMAIN."/api/byocustomapi/profilegridjourneylike.php?loggedin_memberid=14847&memberid=${memberid}&journeyid=0&limit=10&offset=$offset",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
	    "accept: application/json",
	    "cache-control: no-cache",
	    "content-type: application/json",
	    "postman-token: 832f51d8-fef5-9112-f3f2-36e84ee688f9",
	    "x-dreamfactory-api-key: 36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88"
	  ),
	));

	$response = curl_exec($curl);
	$arr = json_decode($response,true);
	$err = curl_error($curl);

	curl_close($curl);
	$output = '';
	if(!empty($arr['resource'])){
		foreach ($arr['resource'] as $grid) {

			$finalTitle = $grid['jtitle'];
			if(empty($finalTitle)){
				$finalTitle = $grid['jhead'];
			}
			$fullWidth = $_POST['viewport'];

			if($fullWidth > 768){
				$unit = 9*16*2;
			}elseif ($fullWidth > 1400) {
				$unit = 16*16*2;
			}elseif ($fullWidth > 1600) {
				$unit = 25*16*2;
			}else{
				$unit = 0;
			}

			$lHeight = (($fullWidth+$unit)/4-12)*1.38;

			// $lHeight = ((($fullWidth+15)/2)*1.38)-$unit;
			//$rHeight = (($fullWidth/4)*1.38)-($unit/2);
			$rHeight = ($lHeight/2)-4;

			$output .= '
			<div class="my-5 grid-item pb-0 pt-2" data-isblog="'.($grid['journey_description'] ? "1" : "0").'" data-jid="'.$grid['journeyid'].'" style="padding-left: 20px;padding-right: 20px;">';

			if($grid['journeydays'] > 0){
				if($grid['journeydays'] == 1){
					$output .= '  <div>
			        <small class="text-muted pl-1">'.$grid['journeydays'].' day</small>
			      </div>';
				}else{
					$output .= '  <div>
			        <small class="text-muted pl-1">'.$grid['journeydays'].' days</small>
			      </div>';
				}
			    
			}

		    $output .= '  <div>
		        <p class="w-100 mb-2 font-weight-bold gridTitle pr-3 pl-1"><span class="pr-4">'.urldecode($finalTitle).'</span><small class="float-right font-weight-bold views">'.kFormatter($grid['totalviewcnt']).' views</small></p>
		      </div>
		      <div class="grid">
		        <div class="l1">
		        ';
		    $link1Ext = substr($grid['link1'], -3);
		    if($link1Ext == 'mp4' || $link1Ext == 'mov'){
		    	$output .= '<video loop muted playsinline ref="video" data-jid="'.$grid['journeyid'].'" class="give-height videoplayer img-fluid" controlslist="nodownload" poster="'.videoThumbnail($grid['link1']).'" style="height:inherit;/*height:'.$lHeight.'px;*/">
		              <source type="video/mp4" src="'.videoLink($grid['link1']).'">
		          </video>';
		    }else{
		    	$output .= '<img src="'.imageThumbnail($grid['link1']).'" class="img-fluid" style="/*height:'.$lHeight.'px;*/height:inherit;">';
		    }
		          
		    $output .= '    </div><div class="rDiv">';
		    if(!empty($grid['link2'])){
		        $output .= '<div class="r1" style="background:url(\''.imageThumbnail($grid['link2']).'\');"></div>';
		    }
		    if(!empty($grid['link4'])){
		        $output .= '<div class="r2" style="background:url(\''.imageThumbnail($grid['link4']).'\');"></div>';
		    }
		    if(!empty($grid['link3'])){
		        $output .= '<div class="r3" style="background:url(\''.imageThumbnail($grid['link3']).'\');"></div>';
		    }
		    if(!empty($grid['link5'])){
		        $output .= '<div class="r4" style="background:url(\''.imageThumbnail($grid['link5']).'\');"></div>';
		    }

		    $output .= '
		      </div></div>';
		    if($grid['jrnylikecnt'] > 0){
	        	$output .= '<p class="d-none pt-3 px-2 mb-0" style="font-weight: 300;font-size: 0.85rem;">
	        				<img src="assets/imgs/'.($grid['isLikedMemId'] ? 'inspire_yellow_i.png' : 'inspire_black_i.png').'" class="img-fluid mr-2 inspire-icon">
	        				Inspired by '.($grid['jrnylikecnt'] > 1 ? '<strong style="font-weight: 700;">---</strong> and <strong style="font-weight: 700;">'.($grid['jrnylikecnt']-1).' others</strong>' : '<strong style="font-weight: 700;">---</strong>').'
	        			</p>';
			}

		      if(!empty($grid['journey_description'])){
		      	$output .= '<p style="word-break: break-word;/*overflow-wrap: anywhere;/*line-break: anywhere;*/       /* height is 2x line-height, so two lines will display */overflow: hidden;text-overflow: ellipsis;display: -webkit-box;-webkit-box-orient: vertical;-webkit-line-clamp: 2; /* number of lines to show */line-height: 1.5;        /* fallback */max-height: 3.5em;font-family: \'Josefin Sans\', sans-serif;" class="pt-3 px-2">'.urldecode($grid['journey_description']).'</p>';
		      }
		    $output .= '</div>
			';
		}
	}

	echo $output;
}

if($method == "markers" && !empty($_POST['data'])){
	header('Content-Type: application/json');
	$mid = $_POST['data']['mid'];
	$jid = $_POST['data']['jid'];
	$returnData['result'] = true;

	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => ENV_PROTOCOL."df.".ENV_DOMAIN."/api/v2/shoutprod96/_proc/pvtvglobalfeedtagR3J1617?wrapper=resource",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "POST",
	  CURLOPT_POSTFIELDS => "{\"params\": [\"806\", \"$mid\", \"$jid\", \"\"]}",
	  CURLOPT_HTTPHEADER => array(
	    "accept: application/json",
	    "cache-control: no-cache",
	    "content-type: application/json",
	    "postman-token: 0bec1d57-4ae2-86b5-35fa-27a64e623c41",
	    "x-dreamfactory-api-key: 36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88"
	  ),
	));

	$response = curl_exec($curl);
	$returnData['response'] = json_decode($response,true);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  $returnData['result'] = false;
	}

	echo json_encode($returnData);
	//
}

if($method == 'users' && !empty($_POST['keyword'])){
	$keyword = $_POST['keyword'];
	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => ENV_PROTOCOL."df.".ENV_DOMAIN."/api/v2/shoutprod96/_proc/kprmbrprflistR3J1506?wrapper=resource",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "POST",
	  CURLOPT_POSTFIELDS => "{\"params\":\n\t[\"808\",\"20\",\"0\",\"\",\"%".$keyword."%\"]\n}",
	  CURLOPT_HTTPHEADER => array(
	    "accept: application/json",
	    "cache-control: no-cache",
	    "content-type: application/json",
	    "postman-token: 0bec1d57-4ae2-86b5-35fa-27a64e623c41",
	    "x-dreamfactory-api-key: 36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88"
	  ),
	));

	$response = curl_exec($curl);
	$arr = json_decode($response,true);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	}

	$output = '';
	foreach ($arr['resource'] as $user) {

  		$output .= '<div class="col-12 col-sm-6 mb-4">
	        <div class="media">
			  <img src="'.($user['imageurl'] ? urlToJpg($user['imageurl']) : "male.png").'" class="img-fluid mr-3" style="height: 64px;width: 64px;object-fit: cover;border-radius: 50%;">
			  <div class="media-body">
			    <h6 class="m-0">'.$user['fname'].' '.$user['lname'].'<a href="./?'.str_replace('@', '', $user['account_handle']).'" class="btn btn-sm btn-dark float-right" target="_blank">View</a></h6>
			    '.$user['account_handle'].'
			  </div>
			</div>
		</div>';

	}
	echo $output;
}

if($method == 'inviteLink' && $_POST['rid'] > 0){
	$rid = $_POST['rid'];
	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => ENV_PROTOCOL."df.".ENV_DOMAIN."/api/v2/shoutprod96/_table/tvinvitecodemst_r3j1489?filter=memberid%3D${rid}",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
	    "accept: application/json",
	    "cache-control: no-cache",
	    "content-type: application/json",
	    "postman-token: 832f51d8-fef5-9112-f3f2-36e84ee688f9",
	    "x-dreamfactory-api-key: 36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88"
	  ),
	));

	$response = curl_exec($curl);
	$arr = json_decode($response,true);
	$err = curl_error($curl);

	curl_close($curl);
	$output = '';
	if(empty($arr['resource'][0]['alias'])){
		if(empty($arr['resource'][0]['invitecode'])){
			$output = "https://travelwithbyo.com/byoexperience/";
		}else{
			$output = "https://travelwithbyo.com/".$arr['resource'][0]['invitecode'];
		}
	}else{
		$output = "https://travelwithbyo.com/".$arr['resource'][0]['alias'];
	}
	echo $output;
}

if($method == 'comments' && $_POST['jid'] > 0){
	$jid = $_POST['jid'];

	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => ENV_PROTOCOL."df.".ENV_DOMAIN."/api/v2/shoutprod96/_proc/pjourneycommentlistr5%28".$jid.",10,0%29?wrapper=resource",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
	    "accept: application/json",
	    "cache-control: no-cache",
	    "content-type: application/json",
	    "postman-token: 832f51d8-fef5-9112-f3f2-36e84ee688f9",
	    "x-dreamfactory-api-key: 36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88"
	  ),
	));

	$response = curl_exec($curl);
	$arr = json_decode($response,true);
	$err = curl_error($curl);

	curl_close($curl);

	$output = '<div class="row no-gutters pt-2 mt-5 mb-3 profile-row comments-section px-2" style="max-height:65vh;overflow-x: hidden;">
  			<h5 class="mb-4">Comments</h5>';
	if(empty($arr['resource'])){
		$output .= '<div class="col-12 mb-4">
			<p class="text-muted">No Comments</p>
			</div>';
	}else{
		foreach ($arr['resource'] as $comment) {
			if(empty($comment['imageurl'])){
				$profileLink = "male.png";
			}else{
				$profileExplode = explode('/upload/', $comment['imageurl']);
				$profileLink = urlToJpg("$profileExplode[0]/upload/w_250,q_auto:low,c_scale/$profileExplode[1]");
			}
			$output .= '
	    		<div class="col-12 mb-2">
	    			<div class="row">
	          			<div class="col-2 col-md-1 pr-2 pr-md-0">
	            			<img src="'.$profileLink.'" class="img-fluid profile-image" style="float: right;height: 40px;width: 40px;border-radius: 50%;object-fit: cover;">
	          			</div>
	          			<div class="col profileRight" style="padding-left: 0;">
	            			<div class="card-body" style="padding-top: 0;padding-left: 0;">
	              				<h6 class="card-title">'.$comment['fname'].'<small class="text-muted d-block" style="font-size: 0.65rem;text-decoration: none;">'.date('d M',strtotime("2020-08-28 06:01:32")).'</small>
	              				</h6>
	              				<p class="card-text">'.urldecode($comment['comment']).'</p>
	              			</div>
	          			</div>
	        		</div>
	    		</div>';
		}
	}
	
	$output .= '</div>';
	echo $output;
}