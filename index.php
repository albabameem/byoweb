<?php
include 'base.php';

$queryString = $_SERVER['QUERY_STRING'];
$rid = 0;
if(empty($queryString)){
	header('Location: https://travelwithbyo.com/');
}else{

	$queryExplode = explode("&", $queryString);
	$account_handle = $queryExplode[0];
	if(isset($queryExplode[1])){
		$journeyExplode = explode('=', $queryExplode[1]);
		if($journeyExplode[0] == "jid"){
			if(isset($journeyExplode[1])){
				$jid = $journeyExplode[1];
			}else{
				header('Location: https://travelwithbyo.com/');
			}
		}elseif($journeyExplode[0] == "ref_id"){
			if(isset($journeyExplode[1]) && is_numeric($journeyExplode[1])){
				$rid = $journeyExplode[1];
			}else{
				$rid = 0;
			}
		}
	}


	if(isset($queryExplode[2])){
		$journeyExplode2 = explode('=', $queryExplode[2]);
		if($journeyExplode2[0] == "jid"){
			if(isset($journeyExplode2[1])){
				$jid = $journeyExplode2[1];
			}else{
				header('Location: https://travelwithbyo.com/');
			}
		}elseif($journeyExplode2[0] == "ref_id"){
			if(isset($journeyExplode2[1]) && is_numeric($journeyExplode2[1])){
				$rid = $journeyExplode2[1];
			}else{
				$rid = 0;
			}
		}
	}

	if(isset($jid)){
		//start blog
		if(is_numeric($jid) && $jid > 0){
			include 'blog.php';
		}else{
			header('Location: https://travelwithbyo.com/');
		}
	}else{
		include 'profile.php';
	}	//end if numeric
}	//end if querystring check
?>