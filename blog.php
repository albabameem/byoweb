<?php
	//start blog

	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => ENV_PROTOCOL."df.".ENV_DOMAIN."/api/v2/shoutprod96/_table/kprusermst?filter=account_handle%3D%40".$account_handle."&fields=memberid%2Cimageurl%2Cfname%2Clname",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
	    "accept: application/json",
	    "cache-control: no-cache",
	    "content-type: application/json",
	    "postman-token: 832f51d8-fef5-9112-f3f2-36e84ee688f9",
	    "x-dreamfactory-api-key: 36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88"
	  ),
	));

	$userResponse = curl_exec($curl);
	$userArray = json_decode($userResponse,true);
	$userError = curl_error($curl);

	curl_close($curl);

	if ($userError) {
		header('Location: https://travelwithbyo.com/');
	} else {
	  	if($userArray['resource'][0]['memberid'] > 0){
			$mid = $userArray['resource'][0]['memberid'];
			if(empty($userArray['resource'][0]['imageurl'])){
				$profileLink = "male.png";
			}else{
				$profileExplode = explode('/upload/', $userArray['resource'][0]['imageurl']);
				$profileLink = urlToJpg("$profileExplode[0]/upload/w_250,q_auto:low,c_scale/$profileExplode[1]");
			}
			$fname = $userArray['resource'][0]['fname'];
			$lname = $userArray['resource'][0]['lname'];
		}else{
			header('Location: https://travelwithbyo.com/');
		}
	}



	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => ENV_PROTOCOL.ENV_DOMAIN."/api/byocustomapi/profilegridjourney.php?memberid=$mid&journeyid=$jid&limit=100&offset=0",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
	    "accept: application/json",
	    "cache-control: no-cache",
	    "content-type: application/json",
	    "postman-token: 832f51d8-fef5-9112-f3f2-36e84ee688f9",
	    "x-dreamfactory-api-key: 36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88"
	  ),
	));

	$blogResponse = curl_exec($curl);
	$blogArray = json_decode($blogResponse,true);
	$blogError = curl_error($curl);

	curl_close($curl);

	if ($blogError) {
	  echo "cURL Error #:" . $blogError;
	}

	$finalTitle = urldecode($blogArray['resource'][0]['jtitle']);
	if(empty($finalTitle)){
		$finalTitle = urldecode($blogArray['resource'][0]['jhead']);
	}

	$grid = $blogArray['resource'][0];

	if(empty($grid)){
		header('Location: ./?'.$account_handle);
	}
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@300&family=Dancing+Script:wght@400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <meta name="keywords" content="social network, wanderlust, travel, community platform, mobile app, location based, travel app, digital nomads">
    <meta name="description" content="<?=urldecode($grid['journey_description']);?>">

    <!-- Twitter Card Meta Data -->
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="byo"/>
	<meta name="twitter:url" content="https://travelwithbyo.com/profile?<?=$account_handle;?>&jid=<?=$jid;?>"/>
	<meta name="twitter:domain" content="https://byo.social"/>
	<meta name="twitter:creator" content="byo"/>
	<meta name="twitter:description" content="<?=urldecode($grid['journey_description']);?>"/>
	<meta name="twitter:title" content="<?=$finalTitle;?> | byo"/>
	<meta name="twitter:image" content="<?=imgMani($grid['journey_thumb'], 'low', '450', true);?>"/>
	<meta name="twitter:image:alt" content="<?=imgMani($grid['journey_thumb'], 'low', '450', true);?>"/>



	<!-- Facebook OG Meta Data -->
	<meta property="og:site_name" content="byo"/>
	<meta property="og:type" content="website" />
	<meta property="og:description" content="<?=urldecode($grid['journey_description']);?>" />
	<meta property="og:title" content="<?=$finalTitle;?> | byo" />
	<meta property="og:image" content="<?=imgMani($grid['journey_thumb'], 'low', '450', true);?>" />
	<meta property="og:image:alt" content="<?=imgMani($grid['journey_thumb'], 'low', '450', true);?>" />
	<meta property="og:url" content="https://travelwithbyo.com/profile?<?=$account_handle;?>&jid=<?=$jid;?>"/>
	<meta property="og:image:height" content="400" />
	<meta property="og:image:width" content="800" />
	<meta property="og:see_also" content="https://travelwithbyo.com"/>
	<meta property="fb:app_id" content="976708549107790" />

    <title><?=$finalTitle;?> | <?=$fname;?>'s blog on byo</title>
    <link rel="shortcut icon" type="image/png" href="https://storage.googleapis.com/byocdn/imagetranscoding/assets/images/icon_64.png">

    <style type="text/css">
		html,body{
			scroll-behavior: smooth;
		    /*background-color: #fafafa;*/
		}
    	::-webkit-scrollbar {
		    width: 0px;  /* Remove scrollbar space */
		    background: transparent;  /* Optional: just make scrollbar invisible */
		}
		/* Optional: show position indicator in red */
		::-webkit-scrollbar-thumb {
		    background: transparent;
		}
		*::-webkit-media-controls-panel {
		  display: none!important;
		  -webkit-appearance: none;
		}

		*::-webkit-media-controls-play-button {
		  display: none!important;
		  -webkit-appearance: none;
		}

		*::-webkit-media-controls-start-playback-button {
		  display: none!important;
		  -webkit-appearance: none;
		}
		*::-webkit-media-controls-fullscreen-button {
		    display: none;
		}
		::-webkit-media-controls {
		  display:none !important;
		}

		#map {
			width: 100%;
		    height: 400px;
		    display: none;
		}

		button:active, button:focus, input:active, input:focus{
		    outline: 0!important;
		    box-shadow: none !important;
		    border-color: #ced4da!important;
		}

		.heading{
			font-family: 'Dancing Script', cursive;
		}

		.content{
			font-family: 'Josefin Sans', sans-serif;
		}


    	#topBar{
    		background-color: #fff;
			padding: 0.7rem 20px !important;
		    display: flex;
		    border-bottom: 1px solid #eee;
		    width: 100%;
		    justify-content: space-between;
    	}

    	h3.heading{
    		position: sticky;
		    top: 0;
		    z-index: 99;
		    padding: 0.7rem 0;
    	}

    	.profile-row.d-lg-block{
    		right: 3rem!important;
    	}
    	
		.grid-item,.profile-row{
			cursor: pointer;
			position: relative;
		}

		.grid-days{
			display: flex;
		    justify-content: center;
		    position: absolute;
		    width: 100%;
		    bottom: -1rem;
		}

		.grid-days > span{
		    box-shadow: 0 0 0px 1px #e8e7e7;
		    width: 3rem;
		    background: #fff;
		    font-family: 'Dancing Script', cursive;
		    text-align: center;
		    border-radius: 50%;
		    border: 0;
		    font-size: 0.75rem;
		    line-height: 1.2;
		    display: flex;
		    justify-content: center;
		    align-items: center;
		    font-weight: bold;
		    text-transform: capitalize;
		    
		}

		.grid{
			/*display: grid;
		    grid-template-columns: 2fr 1fr 1fr;
		    grid-template-rows: 1fr 1fr;
		    gap: 6px 6px;
	        margin-right: -6px;
		    grid-template-areas:
		        "l1 r1 r2 ."
		        "l1 r3 r4 ."
		        ". . . .";
		        */
		    display: flex;
		    flex-wrap: wrap;
		}

		.rDiv{
			flex-basis: calc(50% - 3px);height:auto;display: flex;flex-wrap: wrap;margin-left: 3px;
		}

		.r1 { /*grid-area: r1;*/background-size: cover !important;border-radius: 10px;flex-basis: calc(50% - 3px);height: calc(50% - 3px);margin-right: 3px;margin-bottom: 3px; }

		.r2 { /*grid-area: r2;*/background-size: cover !important;border-radius: 10px;height: calc(50% - 3px);flex-basis: calc(50% - 3px);margin-left: 3px;margin-bottom: 3px; }

		.r3 { /*grid-area: r3;*/background-size: cover !important;border-radius: 10px;flex-basis: calc(50% - 3px);height: calc(50% - 3px);margin-top: 3px;margin-right: 3px; }

		.r4 { /*grid-area: r4;*/background-size: cover !important;border-radius: 10px;flex-basis: calc(50% - 3px);height: calc(50% - 3px);margin-top: 3px;margin-left: 3px;  }

		.r1 img, .r2 img, .r3 img, .r4 img{
			border-radius: 10px;
		    object-fit: cover;
		    width: 100%;
		}

		.l1 { /*grid-area: l1;*/flex-basis: calc(50% - 3px);margin-right: 3px;height: calc(100% - 20vh);}

		.l1.shine{
			height: 34vh;
		}

		.l1 video, .l1 img{
			border-radius: 10px;
		    width: 100%;
		    object-fit: cover;
		}

		.gridTitle{
			display: flex;
			justify-content: space-between;
		}

		.gridTitle > span{
			display: inline-block;
			font-size: 1rem;
		    /*width: 75%;*/
		    overflow: hidden;
		    white-space: nowrap;
		    text-overflow: ellipsis;
		}

		.gridTitle .views{
			font-size: 0.8rem;
			white-space: nowrap;
			align-self: flex-end;
		}
		
		@media (min-width: 768px){
		
			#topBar{
				padding: 0.7rem 9rem;
			}

			body{
				padding: 0 9rem;
			}


			#topBar > img{
			    max-width: 25% !important;
			}

			.profileRight{
				padding-left: 15px!important;
			}

			.comment-box{
				position: static !important;
			}

			.comment-box .input-group{
				width: 80%;
			}
		}

		@media (min-width: 1000px){

			body{
				padding: 0 14rem;
			}

			#topBar{
				padding: 0.7rem 14rem;
			}

			.r1 img, .r2 img, .r3 img, .r4 img{
				border-radius: 12px;
			}

			.l1 video, .l1 img{
				border-radius: 12px;
			}
			.l1.shine{
				height: 65vh;
			}

		}

		@media (min-width: 1200px){

			body{
				padding: 0 16rem;
			}

			#topBar{
				padding: 0.7rem 16rem;
			}



		}



		@media (min-width: 1600px){

	    	.profile-row.d-lg-block{
	    		right: 10rem!important;
	    	}

			#topBar{
				padding: 0.7rem 25rem;
			}

			body{
				padding: 0 25rem;
			}
		}
    </style>
  </head>
  <body data-offset="0" data-loaded="0">
  	<div id="topBar">
  		<img src="logo.svg" class="img-fluid" style="max-width: 40%;object-fit: contain;">
  		<a href="javascript:void(0);" class="download-link btn btn-primary px-2 btn-sm" style="display: inline-block;border-radius: 4px;background-color: #37a3ff;border-color: #37a3ff;align-self: center;">Download App</a>
  	</div>

  	<h3 class="heading text-center mt-4 px-2"><?=$finalTitle;?></h3>

  	<div class="container-fluid" id="topSection" style="padding-left: 20px;padding-right: 20px;position: relative;">
  		<div class="row no-gutters d-none d-lg-block my-4 profile-row" style="position: fixed;z-index: 9;margin-bottom: 0 !important;bottom: 0.25rem;">
  			<span style="font-size: 0.8rem;font-weight: 600;">View more experiences from</span>
  			<div class="col-12 justify-content-center mt-2">
    			<div class="row">
          			<div class="col-6" style="padding-right: 10px;">
            			<img src="<?=$profileLink;?>" class="img-fluid profile-image" style="height:64px;width:64px;border-radius: 50%;object-fit: cover;float: right;">
          			</div>
          			<div class="col-6 profileRight" style="padding-left: 0;padding: 0!important;">
            			<div class="card-body" style="padding-top: 0.5rem;padding-left: 0;">
              				<h6 class="card-title"><?=$userArray['resource'][0]['fname'];?><a href="?<?=$account_handle;?><?=($rid ? "&ref_id=$rid" : "");?>" class="text-muted d-block pt-1" style="font-size: 0.65rem;text-decoration: none;">@<?=$account_handle;?></a>
              				</h6>
              			</div>
          			</div>
        		</div>
    		</div>
        </div>
  		<div class="mt-3 grid-item open-popup" data-jid="<?=$grid['journeyid'];?>">
  		<?php
  		$output = '';


	    $output .= '  <div>
	        <p class="w-100 mb-2 font-weight-bold gridTitle pr-3 d-none"><span class="pr-4"></span><small class="float-right font-weight-bold views">'.kFormatter($grid['totalviewcnt']).' views</small></p>
	      </div>
	      <div class="grid">
	        <div class="l1">
	        ';
	    $link1Ext = substr($grid['link1'], -3);
	    if($link1Ext == 'mp4' || $link1Ext == 'mov'){
	    	$output .= '<video loop muted playsinline ref="video" data-jid="'.$grid['journeyid'].'" class="give-height videoplayer img-fluid" controlslist="nodownload" poster="'.videoThumbnail($grid['link1']).'" style="height:inherit;">
	              <source type="video/mp4" src="'.videoLink($grid['link1']).'">
	          </video>';
	    }else{
	    	$output .= '<img src="'.imageThumbnail($grid['link1']).'" class="img-fluid" style="height:inherit;">';
	    }
	          
	    $output .= '    </div><div class="rDiv">';
	    if(!empty($grid['link2'])){
	        $output .= '<div class="r1" style="background:url(\''.imageThumbnail($grid['link2']).'\');"></div>';
	    }
	    if(!empty($grid['link4'])){
	        $output .= '<div class="r2" style="background:url(\''.imageThumbnail($grid['link4']).'\');"></div>';
	    }
	    if(!empty($grid['link3'])){
	        $output .= '<div class="r3" style="background:url(\''.imageThumbnail($grid['link3']).'\');"></div>';
	    }
	    if(!empty($grid['link5'])){
	        $output .= '<div class="r4" style="background:url(\''.imageThumbnail($grid['link5']).'\');"></div>';
	    }

	    $output .= '
	      </div></div>
		';
		echo $output;

		if($grid['journeydays'] > 0){
			echo '<div class="grid-days d-none">';
			if($grid['journeydays'] == 1){
				echo '<span class="p-4">'.$grid['journeydays'].' day</span>';
			}else{
				echo '<span class="p-4">'.$grid['journeydays'].' days</span>';
			}
			echo '</div';
		}

  		?>
	    </div>

  	</div>
  	</div>

  	<div class="container-fluid pt-2" style="padding-left: 20px;padding-right: 20px;">
  		<p class="text-center text-md-left content pb-4 pt-0 mb-4 mt-4  px-4 journey-para" style="word-break: break-word;font-size: 1.25rem;min-height: 20vh;/*overflow-wrap: anywhere;/*line-break: anywhere;*/">
  			<?=urldecode($grid['journey_description']);?>
  		</p>
  		<div class="px-5 mb-5">
  			<div  id="map"></div>
  		</div>
  		<div class="row no-gutters d-lg-none pt-2 mt-5 mb-3 profile-row">
  			<div class="col-12 text-center mb-2" style="font-weight: 600;font-size: 0.75rem;">View more experiences from</div>
    		<div class="col-12 justify-content-center">
    			<div class="row">
          			<div class="col-6">
            			<img src="<?=$profileLink;?>" class="img-fluid profile-image" style="height:58px;width:58px;border-radius: 50%;object-fit: cover;float: right;">
          			</div>
          			<div class="col-6 profileRight" style="padding-left: 0;">
            			<div class="card-body" style="padding-top: 0.5rem;padding-left: 0;">
              				<h6 class="card-title"><?=$userArray['resource'][0]['fname'];?><a href="?<?=$account_handle;?><?=($rid ? "&ref_id=$rid" : "");?>" class="text-muted d-block pt-1" style="font-size: 0.65rem;text-decoration: none;">@<?=$account_handle;?></a>
              				</h6>
              			</div>
          			</div>
        		</div>
    		</div>
        </div>
  	</div>


  	<div class="container-fluid py-3 comment-box" style="padding-left: 20px;padding-right: 20px;position: sticky;bottom: 0;background:#fff;">
  		<div class="input-group" style="/*margin: 0 auto;*/">
  			<input type="text" class="form-control load-comments" placeholder="Ask me anything" aria-label="Recipient's username" readonly aria-describedby="basic-addon2" style="border-top-left-radius: 22px;border-bottom-left-radius: 22px;border-right: 0;background: none;">
  			<div class="input-group-append load-comments-pseudo">
    			<span class="input-group-text " id="repCount" style="background: transparent;border-top-right-radius: 22px;border-bottom-right-radius: 22px;font-size: 0.75rem;    border-left: 0!important;"></span>
  			</div>
		</div>
  	</div>
    

  	<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle">
	  	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		  	<div class="modal-content p-4 d-none d-lg-block">
		    	<div class="modal-header" style="border: 0;">
	        		<img src="logo.svg" class="img-fluid" style="width: 35%;">
	        	</div>
	        	<div class="modal-body">
	        		<div class="row">
	    				<div class="col justify-content-center pr-5" style="align-self: center;border-right: 2px solid;">
					        <div style="height: 100%;display: flex;flex-wrap: wrap;align-content: center;" class="justify-content-center">
					        	<h2 style="text-align: center;" class="px-4 ">Find your next big adventure with byo</h2>
								<div style="text-align:center;" class="py-4 mb-4">
									<a href="javascript:void(0);" class="download-link px-2 mr-1 " style="display: inline-block;border-radius: 4px;align-self: center;width: 40%;"><img src="PlayStoreIcon.jpg" class="img-fluid"></a>
									<a href="javascript:void(0);" class="download-link px-2 ml-1 " style="display: inline-block;border-radius: 4px;align-self: center;width: 40%;"><img src="AppStoreIcon.jpg" class="img-fluid"></a>
								</div>
							</div>
						</div>
						<div class="col justify-content-center text-center" style="align-self: center;">
							<figure>
								<img src="screen.png" class="img-fluid mb-5" style="max-height: 500px;padding: 0 5rem;">
							</figure>
						</div>
	   				</div>
				</div>
	       	</div>
		    <div class="modal-content d-lg-none">
		       	<div class="modal-body px-5">
			        <div style="text-align:center;" class="my-5">
			        	<img src="logo.svg" class="img-fluid">
			        </div>
			        <h4 style="text-align: center;" class="px-4 my-5">Find your next big adventure with byo</h4>
					<div style="text-align:center;" class="py-3 mb-5">
						<a href="javascript:void(0);" class="download-link btn btn-primary px-2" style="display: inline-block;border-radius: 4px;background-color: #37a3ff;border-color: #37a3ff;align-self: center;">Download App</a>
					</div>
		      	</div>
		    </div>
	  	</div>
	</div>
	

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script>
		// Initialize and add the map
		const myLatLng = { lat: 45.420422, lng: -75.692429 };
		let map;
		let markers = [];

		function addMarker(location, icon) {
		  const marker = new google.maps.Marker({
		    position: location,
		    map: map,
		    animation: google.maps.Animation.DROP,
		    icon: icon
		  });
		  markers.push(marker);
		  
		}

		function setMapOnAll(map) {
		  for (let i = 0; i < markers.length; i++) {
		    markers[i].setMap(map);
		  }
		}

		function clearMarkers() {
		  setMapOnAll(null);
		}

		function deleteMarkers() {
		  clearMarkers();
		  markers = [];
		}

		function initMap() {
		  	map = new google.maps.Map(
		    	document.getElementById('map'), {
		    		zoom: 2,
		    		center: {lat:0.000000, lng: 0.000000},
		    		styles: [
							    {
							        "featureType": "administrative.country",
							        "elementType": "geometry.stroke",
							        "stylers": [
							            {
							                "color": "#908f94"
							            },
							            {
							                "weight": "0.30"
							            },
							            {
							                "visibility": "on"
							            }
							        ]
							    },
							    {
							        "featureType": "administrative.province",
							        "elementType": "all",
							        "stylers": [
							            {
							                "visibility": "off"
							            }
							        ]
							    },
							    {
							        "featureType": "administrative.neighborhood",
							        "elementType": "labels",
							        "stylers": [
							            {
							                "visibility": "off"
							            }
							        ]
							    },
							    {
							        "featureType": "landscape.natural.landcover",
							        "elementType": "geometry.fill",
							        "stylers": [
							            {
							                "visibility": "on"
							            },
							            {
							                "color": "#fffffd"
							            },
							            {
							                "saturation": "-35"
							            }
							        ]
							    },
							    {
							        "featureType": "landscape.natural.terrain",
							        "elementType": "geometry.fill",
							        "stylers": [
							            {
							                "color": "#d0e7bb"
							            }
							        ]
							    },
							    {
							        "featureType": "landscape.natural.terrain",
							        "elementType": "geometry.stroke",
							        "stylers": [
							            {
							                "color": "#e6e4e4"
							            }
							        ]
							    },
							    {
							        "featureType": "poi",
							        "elementType": "all",
							        "stylers": [
							            {
							                "visibility": "off"
							            }
							        ]
							    },
							    {
							        "featureType": "poi.attraction",
							        "elementType": "all",
							        "stylers": [
							            {
							                "visibility": "on"
							            }
							        ]
							    },
							    {
							        "featureType": "poi.attraction",
							        "elementType": "labels.icon",
							        "stylers": [
							            {
							                "visibility": "off"
							            }
							        ]
							    },
							    {
							        "featureType": "poi.business",
							        "elementType": "all",
							        "stylers": [
							            {
							                "visibility": "off"
							            }
							        ]
							    },
							    {
							        "featureType": "poi.park",
							        "elementType": "all",
							        "stylers": [
							            {
							                "visibility": "on"
							            }
							        ]
							    },
							    {
							        "featureType": "poi.park",
							        "elementType": "geometry.fill",
							        "stylers": [
							            {
							                "color": "#d0e7bb"
							            }
							        ]
							    },
							    {
							        "featureType": "poi.park",
							        "elementType": "labels.icon",
							        "stylers": [
							            {
							                "visibility": "off"
							            }
							        ]
							    },
							    {
							        "featureType": "poi.place_of_worship",
							        "elementType": "all",
							        "stylers": [
							            {
							                "visibility": "on"
							            }
							        ]
							    },
							    {
							        "featureType": "poi.place_of_worship",
							        "elementType": "labels.icon",
							        "stylers": [
							            {
							                "visibility": "off"
							            }
							        ]
							    },
							    {
							        "featureType": "poi.sports_complex",
							        "elementType": "all",
							        "stylers": [
							            {
							                "visibility": "on"
							            }
							        ]
							    },
							    {
							        "featureType": "poi.sports_complex",
							        "elementType": "labels.icon",
							        "stylers": [
							            {
							                "visibility": "off"
							            }
							        ]
							    },
							    {
							        "featureType": "road.highway",
							        "elementType": "all",
							        "stylers": [
							            {
							                "visibility": "simplified"
							            },
							            {
							                "color": "#f8cbac"
							            },
							            {
							                "weight": "0.30"
							            }
							        ]
							    },
							    {
							        "featureType": "road.highway",
							        "elementType": "geometry.stroke",
							        "stylers": [
							            {
							                "color": "#c2baba"
							            }
							        ]
							    },
							    {
							        "featureType": "road.highway",
							        "elementType": "labels.icon",
							        "stylers": [
							            {
							                "visibility": "off"
							            }
							        ]
							    },
							    {
							        "featureType": "transit",
							        "elementType": "all",
							        "stylers": [
							            {
							                "visibility": "off"
							            }
							        ]
							    },
							    {
							        "featureType": "transit",
							        "elementType": "geometry.fill",
							        "stylers": [
							            {
							                "color": "#dddbda"
							            }
							        ]
							    },
							    {
							        "featureType": "transit.station",
							        "elementType": "labels.icon",
							        "stylers": [
							            {
							                "visibility": "off"
							            }
							        ]
							    },
							    {
							        "featureType": "water",
							        "elementType": "geometry.fill",
							        "stylers": [
							            {
							                "color": "#72cdf0"
							            }
							        ]
							    },
							    {
							        "featureType": "water",
							        "elementType": "labels.text",
							        "stylers": [
							            {
							                "color": "#030303"
							            }
							        ]
							    }
							],
					disableDefaultUI: true,
					gestureHandling: 'none',
			        zoomControl: false
		    	}
		   	);

			$.ajax({
				method: "POST",
				url: "api.php",
				data: { data: {"mid": <?=$mid;?>, "jid": <?=$jid;?>}, method: "markers"}
			}).done(function( data ) {
				if(data.result){
					var first, last;
					$.each(data.response.resource, function(index, val){
						if(index === 0){
							first = {lat: parseFloat(val.gclat), lng: parseFloat(val.gclng)};
						}
						addMarker({lat: parseFloat(val.gclat), lng: parseFloat(val.gclng)},{url: 'https://travelwithbyo.com/UploadActIcon/'+val.activityiconURL, scaledSize: new google.maps.Size(50, 50)});
						last = {lat: parseFloat(val.gclat), lng: parseFloat(val.gclng)};
					});

					var bounds = new google.maps.LatLngBounds();
					for(i=0;i<markers.length;i++) {
					   bounds.extend(markers[i].getPosition());
					}

					//map.panTo(bounds.getCenter());

					map.fitBounds(bounds);
					if(first.lat === last.lat && first.lng === last.lng){
						map.setZoom(8);
					}else{
						map.setZoom(map.getZoom()+1);
					}
					
					map.fitBounds(map.getBounds());
				}else{
					console.log("error fetching data");
				}
			});

		}
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB5ETecW25ln0et7l7YcFR3M3rnuZ7o90A&callback=initMap"></script>
    <script type="text/javascript">
    	$(window).scroll(function(){
    		let headingOffset = $('#topSection').offset().top;
    		if($(window).scrollTop() > headingOffset - 75){
    			$('h3.heading').css('background', '#fff');
    		}else{
    			$('h3.heading').css('background', 'transparent');
    		}

        	$('video').each(function(){
	            if (IsInViewport($(this))) {
	                var videos = document.querySelectorAll('video');
	                for (var i = 0; i < videos.length; i++) {
	                    videos[i].pause();
	                }
	                $(this)[0].play();
	            } else {
	                $(this)[0].pause();
	            }
        	});
    	});


    	$(document).on('click', '.download-link', function(e){
    		e.stopPropagation();
    		e.preventDefault();
    		if($(this).attr('href') == 'javascript:void(0);'){
    			<?php
    			if($rid > 0){
    			}else{
    				echo "window.open('https://travelwithbyo.com/byoexperience/','_blank');";
    			}
    			?>
    		}else{
    			window.open($(this).attr('href'),'_blank');
    		}
    	});

    	$(document).on('click', '.load-comments-pseudo', function(){
    		$('.load-comments').trigger('click');
    	});


    	$(document).on('click', '.load-comments', function(){
    		var elem = $(this);
    		elem.removeClass('load-comments');
    		$.ajax({
				method: "POST",
				url: "api.php",
				data: { jid: "<?=$jid;?>", method: "comments"}
			}).done(function( msg ) {
				$('.profile-row.d-lg-none').hide();
				$('#map').parent().after(msg);
				$('.profile-image').each(function(){
				    if(parseInt($(this).width())>parseInt($(this).height())){
				        $(this).css('height', $(this).css('width'));
				    }else{
				        $(this).css('width', $(this).css('height'));
				    }
				});
				$([document.documentElement, document.body]).animate({
			        scrollTop: $(".comments-section").offset().top-100
			    }, 500);
			    elem.parent().parent().css('position','static');
				elem.css({'border-top-left-radius': '14px', 'border-bottom-left-radius': '14px', 'height': '5rem'});
				elem.removeClass('load-comments').parent().addClass('open-popup');
				$('#repCount').css({'border-top-right-radius': '14px', 'border-bottom-right-radius': '14px'});
				
			});
    	});

    	$(document).ready(function(){
    		$('#map').prepend('<img src="FrameWebBig.png" class="img-fluid" style="position: absolute;left: 0;height: 400px;top: 0;z-index: 9;width: 100%;">');
    		// let topbarHeight = parseFloat($('#topBar').css('height'))+10;
			// $('#topSection').css('margin-top',topbarHeight+'px');
			$('.profile-image').each(function(){
			    if(parseInt($(this).width())>parseInt($(this).height())){
			        $(this).css('height', $(this).css('width'));
			    }else{
			        $(this).css('width', $(this).css('height'));
			    }
			});
			$('.grid-days > span').css('height', $('.grid-days > span').css('width'));

			$( ".grid img,.grid video" ).each(function() {
				let newHeight = parseFloat($(this).css('width'))*1.38;
				if($(this).parent().hasClass('l1')){
					newHeight = newHeight+15;
				}
				$('.l1').css('height',newHeight+'px');
			});
			$.ajax({
				method: "POST",
				url: "api.php",
				data: { jid: "<?=$jid;?>", method: "repCount"}
			}).done(function( msg ) {
				if(parseInt(msg) > 0){
					$('#repCount').html(msg+' replies');
				}
			});

			$.ajax({
				method: "POST",
				url: "api.php",
				data: { rid: "<?=$rid;?>", method: "inviteLink"}
			}).done(function( msg ) {
				if(msg != ''){
					$('.download-link').attr('href', msg);
				}
			});
    	});

    	$(document).on('click', '.open-popup', function(){
    		$('#myModal').modal('show');
    	});

    	$('.profile-row').click(function(){
    		window.location.href = '?<?=$account_handle;?>'<?=($rid ? "+'&ref_id=$rid'" : "");?>;
    	});

      	var IsInViewport = function(el) {
          if (typeof jQuery === "function" && el instanceof jQuery) el = el[0];
          var rect = el.getBoundingClientRect();
          return (
              rect.top >= 0 &&
              rect.left >= 0 &&
              rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
              rect.right <= (window.innerWidth || document.documentElement.clientWidth)
          );
      	};

    </script>
  </body>
</html>