<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://df.travelwithbyo.com/api/v2/shoutprod96/_proc/kprmbrprflistR3J1506?wrapper=resource",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\"params\":\n\t[\"808\",\"20\",\"0\",\"\",\"%%\"]\n}",
  CURLOPT_HTTPHEADER => array(
    "accept: application/json",
    "cache-control: no-cache",
    "content-type: application/json",
    "postman-token: 0bec1d57-4ae2-86b5-35fa-27a64e623c41",
    "x-dreamfactory-api-key: 36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88"
  ),
));

$response = curl_exec($curl);
$arr = json_decode($response,true);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
}

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

    <title>Hello, world!</title>
    <style type="text/css">
    	.bio{
    		width: 100%;text-align: center;padding: 0 3rem;
    	}
    	.modal-dialog {
		  width: 100%;
		  height: 100%;
		  margin: 0;
		  padding: 0;
		}

		.modal-content {
		  height: auto;
		  min-height: 100%;
		  border-radius: 0;
		}

		.grid{
			display: grid;
		    grid-template-columns: 2fr 1fr 1fr;
		    grid-template-rows: 1fr 1fr;
		    gap: 8px 8px;
		    grid-template-areas:
		        "l1 r1 r2 ."
		        "l1 r3 r4 ."
		        ". . . .";
		}

		.r1 { grid-area: r1;margin-right: -4px; }

		.r2 { grid-area: r2;margin-left: 4px;margin-right: -8px; }

		.r3 { grid-area: r3;margin-right: -4px; }

		.r4 { grid-area: r4;margin-left: 4px;margin-right: -8px;  }

		.r1 img, .r2 img, .r3 img, .r4 img{
			border-radius: 8px;
		    object-fit: cover;
		    width: 100%;
		}

		.l1 { grid-area: l1; }
		.l1 video, .l1 img{
			border-radius: 8px;
		    width: 100%;
		    object-fit: cover;
		}

		@media (min-width: 768px){
			body{
				padding: 0 9rem;
			}

			.bio{
				padding: 0 10rem;
			}
		}


		@media (min-width: 1400px){
			body{
				padding: 0 16rem;
			}
		}


		@media (min-width: 1600px){
			body{
				padding: 0 25rem;
			}
		}
    </style>
  </head>
  <body>
  	<div id="spinner" style="position: absolute;left: 0;top: 0;width: 100%;height: 100%;background: rgb(255 255 255 / 70%);z-index: 999;display: none;">
  		<div style="display: flex;justify-content: center;height: inherit;"><img src="spinner.svg" class="img-fluid"></div>
  	</div>
    <div class="container-fluid mt-5" id="topSection">
      <input type="text" class="form-control" id="user" placeholder="Search User">
      <div class="row mt-4" id="usersRow">
      	<?php
      	foreach ($arr['resource'] as $user) {
  		?>
  		<div class="col-12 col-sm-6 mb-4">
	        <div class="media">
			  <img src="<?=($user['imageurl'] ? $user['imageurl'] : "male.png");?>" class="img-fluid mr-3" style="height: 64px;width: 64px;object-fit: cover;border-radius: 50%;">
			  <div class="media-body">
			    <h6 class="m-0"><?=$user['fname'];?> <?=$user['lname'];?><a href="./?<?=str_replace('@', '', $user['account_handle']);?>" class="btn btn-sm btn-dark float-right" target="_blank">View</a></h6>
			    <?=$user['account_handle'];?>
			  </div>
			</div>
		</div>
		<?php
		}
		?>
      </div>
  	</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script type="text/javascript">

      	
	    <?php
	    if(!empty($arr)){
	    ?>

	    $(document).on('change', '#user', function(){
	    	$('#spinner').show();
	    	$.ajax({
					method: "POST",
					url: "api.php",
					data: { keyword: $(this).val(), method: "users"}
				}).done(function( msg ) {
					$('#usersRow').html(msg);
					$('#spinner').hide();
				});
	    });


		<?php
		}
		?>
    </script>
  </body>
</html>