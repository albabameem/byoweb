<?php
	//start profile

	$curl = curl_init();


	curl_setopt_array($curl, array(
	  CURLOPT_URL => ENV_PROTOCOL."df.".ENV_DOMAIN."/api/v2/shoutprod96/_table/kprusermst?filter=account_handle%3D%40".$account_handle."&fields=memberid",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
	    "accept: application/json",
	    "cache-control: no-cache",
	    "content-type: application/json",
	    "postman-token: 832f51d8-fef5-9112-f3f2-36e84ee688f9",
	    "x-dreamfactory-api-key: 36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88"
	  ),
	));

	$response = curl_exec($curl);
	$arr = json_decode($response,true);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
		header('Location: https://travelwithbyo.com/');
	} else {
	  	if($arr['resource'][0]['memberid'] > 0){
			$mid = $arr['resource'][0]['memberid'];
		}else{
			header('Location: https://travelwithbyo.com/');
		}
	}

	$curl = curl_init();


	curl_setopt_array($curl, array(
	  CURLOPT_URL => ENV_PROTOCOL.ENV_DOMAIN."/api/byocustomapi/suggestionuserlist.php?memberid=$mid",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
	    "accept: application/json",
	    "cache-control: no-cache",
	    "content-type: application/json",
	    "postman-token: 832f51d8-fef5-9112-f3f2-36e84ee688f9",
	    "x-dreamfactory-api-key: 36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88"
	  ),
	));

	$newresponse = curl_exec($curl);
	$newarr = json_decode($newresponse,true);
	$newerr = curl_error($curl);

	curl_close($curl);



	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => ENV_PROTOCOL."df.".ENV_DOMAIN."//api/v2/shoutprod96/_proc/userprofiledtlsr3j1640%28808,%20".$mid."%29?wrapper=resource",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
	    "accept: application/json",
	    "cache-control: no-cache",
	    "content-type: application/json",
	    "postman-token: 832f51d8-fef5-9112-f3f2-36e84ee688f9",
	    "x-dreamfactory-api-key: 36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88"
	  ),
	));

	$response = curl_exec($curl);
	$arr = json_decode($response,true);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	}

	if(empty($arr['resource'][0]['imageurl'])){
		$profileLink = "male.png";
	}else{
		$profileExplode = explode('/upload/', $arr['resource'][0]['imageurl']);
		$profileLink = urlToJpg("$profileExplode[0]/upload/w_250,q_auto:low,c_scale/$profileExplode[1]");
	}
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@300&family=Dancing+Script:wght@400;700&display=swap" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="https://storage.googleapis.com/byocdn/imagetranscoding/assets/images/icon_64.png">
    <meta name="keywords" content="social network, wanderlust, travel, community platform, mobile app, location based, travel app, digital nomads">
    <meta name="description" content="<?=urldecode($arr['resource'][0]['short_bio']);?>">

    <!-- Twitter Card Meta Data -->
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="byo"/>
	<meta name="twitter:url" content="https://travelwithbyo.com/profile?<?=$account_handle;?>"/>
	<meta name="twitter:domain" content="https://byo.social"/>
	<meta name="twitter:creator" content="byo"/>
	<meta name="twitter:description" content="<?=urldecode($arr['resource'][0]['short_bio']);?>"/>
	<meta name="twitter:title" content="<?=$arr['resource'][0]['fname'];?>'s Profile | byo"/>
	<meta name="twitter:image" content="<?=$profileLink;?>"/>
	<meta name="twitter:image:alt" content="<?=$profileLink;?>"/>



	<!-- Facebook OG Meta Data -->
	<meta property="og:site_name" content="byo"/>
	<meta property="og:type" content="website" />
	<meta property="og:description" content="<?=urldecode($arr['resource'][0]['short_bio']);?>" />
	<meta property="og:title" content="<?=$arr['resource'][0]['fname'];?>'s Profile | byo" />
	<meta property="og:image" content="<?=$profileLink;?>" />
	<meta property="og:image:alt" content="<?=$profileLink;?>" />
	<meta property="og:url" content="https://travelwithbyo.com/profile?<?=$account_handle;?>"/>
	<meta property="og:image:height" content="400" />
	<meta property="og:image:width" content="800" />
	<meta property="og:see_also" content="https://travelwithbyo.com"/>
	<meta property="fb:app_id" content="976708549107790" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="owlcarousel/css/owl.carousel.min.css">
	<link rel="stylesheet" href="owlcarousel/css/owl.theme.default.min.css">

    <title><?=$arr['resource'][0]['fname'];?>'s Profile | byo</title>
    <style type="text/css">

    	html, body, #topSection .card{
		    background-color: #fafafa;
		    -webkit-overflow-scrolling: touch!important;
    	}
    	::-webkit-scrollbar {
		    width: 0px;  /* Remove scrollbar space */
		    background: transparent;  /* Optional: just make scrollbar invisible */
		}
		::-webkit-scrollbar-thumb {
		    background: transparent;
		}
		*::-webkit-media-controls-panel {
		  display: none!important;
		  -webkit-appearance: none;
		}

		*::-webkit-media-controls-play-button {
		  display: none!important;
		  -webkit-appearance: none;
		}

		*::-webkit-media-controls-start-playback-button {
		  display: none!important;
		  -webkit-appearance: none;
		}
		*::-webkit-media-controls-fullscreen-button {
		    display: none;
		}
		::-webkit-media-controls {
		  display:none !important;
		}
		.shine {
		  background: #f6f7f8;
		  border-radius: 6px;
		  background-image: linear-gradient(to right, #f6f7f8 0%, #edeef1 20%, #f6f7f8 40%, #f6f7f8 100%);
		  background-repeat: no-repeat;
		  background-size: 800px 100%; 
		  display: inline-block;
		  position: relative; 
		  
		  -webkit-animation-duration: 1s;
		  -webkit-animation-fill-mode: forwards; 
		  -webkit-animation-iteration-count: infinite;
		  -webkit-animation-name: placeholderShimmer;
		  -webkit-animation-timing-function: linear;
		  }


		@-webkit-keyframes placeholderShimmer {
		  0% {
		    background-position: -468px 0;
		  }
		  
		  100% {
		    background-position: 468px 0; 
		  }
		}

		button:active, button:focus{
			outline: 0;
		}


    	#topBar{
    		background-color: #fff;
			padding: 0.7rem 20px;
		    display: flex;
		    border-bottom: 1px solid #eee;
		    position: fixed;
		    top: 0;
		    left: 0;
		    z-index: 9;
		    width: 100%;
		    justify-content: space-between;
    	}

    	.bio{
    		width: 100%;text-align: center;padding: 0 3rem;
    	}

		.grid-item{
			cursor: pointer;
		}

		.grid{
			/*display: grid;
		    grid-template-columns: 2fr 1fr 1fr;
		    grid-template-rows: 1fr 1fr;
		    gap: 6px 6px;
	        margin-right: -6px;
		    grid-template-areas:
		        "l1 r1 r2 ."
		        "l1 r3 r4 ."
		        ". . . .";
		        */
		    display: flex;
		    flex-wrap: wrap;
		}

		.rDiv{
			flex-basis: calc(50% - 3px);height:auto;display: flex;flex-wrap: wrap;margin-left: 3px;
		}

		.r1 { /*grid-area: r1;*/background-size: cover !important;border-radius: 10px;flex-basis: calc(50% - 3px);height: calc(50% - 3px);margin-right: 3px;margin-bottom: 3px; }

		.r2 { /*grid-area: r2;*/background-size: cover !important;border-radius: 10px;height: calc(50% - 3px);flex-basis: calc(50% - 3px);margin-left: 3px;margin-bottom: 3px; }

		.r3 { /*grid-area: r3;*/background-size: cover !important;border-radius: 10px;flex-basis: calc(50% - 3px);height: calc(50% - 3px);margin-top: 3px;margin-right: 3px; }

		.r4 { /*grid-area: r4;*/background-size: cover !important;border-radius: 10px;flex-basis: calc(50% - 3px);height: calc(50% - 3px);margin-top: 3px;margin-left: 3px;  }

		.r1 img, .r2 img, .r3 img, .r4 img{
			border-radius: 10px;
		    object-fit: cover;
		    width: 100%;
		}

		.l1 { /*grid-area: l1;*/flex-basis: calc(50% - 3px);margin-right: 3px;height: calc(100% - 20vh);}

		.l1.shine{
			height: 34vh;
		}

		.l1 video, .l1 img{
			border-radius: 10px;
		    width: 100%;
		    object-fit: cover;
		}

		.gridTitle{
			display: flex;
			justify-content: space-between;
		}

		.gridTitle > span{
			display: inline-block;
			font-size: 1rem;
		    /*width: 75%;*/
		    overflow: hidden;
		    white-space: nowrap;
		    text-overflow: ellipsis;
		}

		.gridTitle .views{
			font-size: 0.8rem;
			white-space: nowrap;
			align-self: flex-end;
		}

		.owl-carousel .owl-item img {
		    width: auto;
		}

		.owl-carousel .owl-stage{
		    padding-bottom: 1.5rem!important;
		    padding-top: 1.5rem!important;
		    overflow-x: scroll;
		    -webkit-overflow-scrolling: touch!important;
		    white-space: nowrap;
		    width: auto !important;
	        transform: none !important;
		}

		.owl-carousel.owl-drag .owl-item{
			touch-action: manipulation!important;
			    -ms-touch-action: manipulation!important;
		}


	    .owl-carousel .owl-stage .owl-item{
	    	user-select: none;
	    	display: inline-block;
	    	float: none!important;
	    }

		#suggestedRow{
			margin: 0 auto;
			position: relative;
		}

		#suggestedRow .card{
			cursor: pointer;
		}
		#suggestedRow .card img.card-img-top{
			width: 85px;
			height: 85px;
		}

		.act-img{
			max-height: 2rem;
		}

		#suggestedRow .card-title {
		    height: 60px;
		    overflow: hidden;
		    text-overflow: ellipsis;
		    white-space: nowrap;
		}

		#suggestedRow .card-title small{
			white-space: normal;
		}

		.suggested-act img:last-child{
			margin-right: 0!important;
		}


		#suggestedRow .owl-nav button{
		    position: absolute;
		    top: 40%;
		    background: #fff;
		    box-shadow: 0px 0px 10px 0px #eee;
		    /*padding: 1rem !important;*/
		    border-radius: 50%;
		    width: 3rem;
		    height: 3rem;
		    font-size: 2rem;
		    display: flex;
		    align-items: center;
		    justify-content: center;
		}

		#suggestedRow .owl-prev{
		    left: -1rem;
		}

		#suggestedRow .owl-next{
		    right: -1rem;
		}

		.inspire-icon{
			height: 25px;
		}

		@media (max-width: 400px){

			#suggestedRow .card{
				/*padding-top: 0!important;*/
				padding-bottom: 0!important;
			}

			#suggestedRow .card img.card-img-top{
				width: 64px!important;
				height: 64px!important;
			}

			.act-img{
				max-height: 1.8rem;
			}
		}

		@media (max-width: 500px){
			.profileRight{
				padding-left: 1.2rem !important;
			}
		}

		@media (min-width: 768px){

		
			#suggestedRow .card-title {
			    height: auto;
			}

			#topBar{
				padding: 0.7rem 9rem;
			}

			body{
				padding: 0 9rem;
			}

			/*
			#topBar{
				justify-content: space-evenly;
			}*/

			#topBar > img{
			    max-width: 25% !important;
			}

			.inspire-icon{
				height: 30px;
			}
		}

		@media (min-width: 1000px){

			body{
				padding: 0 14rem;
			}

			#topBar{
				padding: 0.7rem 14rem;
			}

			.r1 img, .r2 img, .r3 img, .r4 img{
				border-radius: 12px;
			}

			.l1 video, .l1 img{
				border-radius: 12px;
			}
			.l1.shine{
				height: 65vh;
			}

			.bio{
				/*padding: 0 10rem;*/
			}
		}

		@media (min-width: 1200px){

			body{
				padding: 0 16rem;
			}

			#topBar{
				padding: 0.7rem 16rem;
			}

		}



		@media (min-width: 1600px){

			#topBar{
				padding: 0.7rem 25rem;
			}

			body{
				padding: 0 25rem;
			}
		}
    </style>
  </head>
  <body data-offset="0" data-loaded="0">
  	<div id="topBar">
  		<img src="logo.svg" class="img-fluid" style="max-width: 40%;object-fit: contain;">
  		<a href="javascript:void(0);" class="download-link btn btn-primary px-2 btn-sm" style="display: inline-block;border-radius: 4px;background-color: #37a3ff;border-color: #37a3ff;align-self: center;">Download App</a>
  	</div>
    <div class="container-fluid pt-4" id="topSection">
      <div class="card mb-3 profileLeft" style="max-width: 540px;border: 0;">
        <div class="row no-gutters">
          <div class="col-4" style="text-align: center;">
            <img src="<?=$profileLink;?>" class="img-fluid profile-image" style="height:115px;width:115px;border-radius: 50%;object-fit: cover;">
          </div>
          <div class="col-8 profileRight">
            <div class="card-body" style="padding-top: 0.25rem;padding-left: 0;">
              <h5 class="card-title"><?=$arr['resource'][0]['fname'];?><small class="text-muted pt-1" style="display: block;"><?=$arr['resource'][0]['account_handle'];?></small></h5>
              <a href="javascript:void(0);" class="open-popup btn btn-primary px-4" style="display: inline-block;border-radius: 22px;padding-left: 1rem;padding-right: 1rem;background-color: #37a3ff;border-color: #37a3ff;margin-top: 1rem;">Follow</a>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <p class="bio"><?=urldecode($arr['resource'][0]['short_bio']);?></p>
      </div>

      <?php
      /*
      if(!empty($arr['resource'][0]['external_link'])){
      ?>
      <div class="row mb-4">
        <a href="<?=urldecode($arr['resource'][0]['external_link']);?>" target="_blank" style="color:#67dfdb;width: 100%;text-align: center;padding: 0 3rem;"><?=urldecode($arr['resource'][0]['external_link']);?></a>
      </div>
      <?php
  	  }


      <div class="row  justify-content-center">
      	<span style="margin-right: 1rem;"><img src="nowdestination_icon.webp" class="img-fluid pr-1" style="max-height: 1rem;"><?=$arr['resource'][0]['destination_now'];?></span>
      	<span style="margin-left: 1rem;"><img src="nextdestination_icon.png" class="img-fluid pr-1" style="max-height: 1rem;"><?=$arr['resource'][0]['destination_next'];?></span>
      </div>
  	  */
  	  ?>

      <div id="interestsRow" class="row justify-content-center mt-3 mb-4">
        
      </div>
    </div>



    

    <div class="container-fluid my-5" id="spinner" style="padding-left: 20px;padding-right: 20px;">
    	<div class="grid">
	        <div class="l1 shine"></div>
	        <div class="rDiv">
		        <div class="r1 shine"></div>
		        <div class="r2 shine"></div>
		        <div class="r3 shine"></div>
		        <div class="r4 shine"></div>
		    </div>
		</div>
    </div>


	<div class="mb-5" style="padding-left: 20px;padding-right: 20px;display: none;">
		<h6 class="font-weight-bold">Suggested for you</h6>
		<div id="suggestedRow" class="owl-carousel row justify-content-center">
			<?php
			foreach ($newarr['resource'] as $user) {
				if(empty($user['imageurl'])){
					$profileLink2 = "male.png";
				}else{
					$profileExplode2 = explode('/upload/', $user['imageurl']);
					$profileLink2 = urlToJpg("$profileExplode2[0]/upload/w_250,q_auto:low,c_scale/$profileExplode2[1]");
				}
				if(empty($user['account_handle']) || empty($user['fname'])){
					continue;
				}
			?>
			<div class="card mx-2 p-4 suggested-card" data-handle="<?=str_replace('@','',$user['account_handle']);?>" style="width: auto;border: 0;box-shadow: 0px 0px 5px 1px #eee;">
		  		<img class="card-img-top" src="<?=$profileLink2;?>" alt="Card image cap" style="object-fit: cover;border-radius: 50%;margin: 0 auto;">
				<div class="card-body px-0 pb-1 pb-md-3" style="text-align: center;padding-bottom: 0!important;">
				   	<h6 class="card-title"><?=substr(urldecode($user['fname']), 0, -3);?><small style="display: block;font-size: 0.75rem;" class="pt-2"><?=($user['account_handle'] ?? "undefined");?></small></h6>
				    <div class="row suggested-act justify-content-center my-4">
					   	<?php
					   	$cnt = 1;
					   	foreach ($user['act'] as $activity) {
					   		if($cnt > 3){
					   			break;
					   		}
					   	?>
				    	<img src="<?=(ENV_PROTOCOL.ENV_DOMAIN.'/UploadActIcon/'.$activity['iconURL']);?>" class="img-fluid mr-3 act-img">
				    	<?php
				    		$cnt++;
						}
						?>
				    </div>
				</div>
			</div>

			<?php
			}
			?>
		</div>
	</div>

    <div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle">
	  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		  	<div class="modal-content p-4 d-none d-lg-block">
		    	<div class="modal-header" style="border: 0;">
	        		<img src="logo.svg" class="img-fluid" style="width: 35%;">
	        	</div>
	        	<div class="modal-body">
	        		<div class="row">
	    				<div class="col justify-content-center pr-5" style="align-self: center;border-right: 2px solid;">
					        <div style="height: 100%;display: flex;flex-wrap: wrap;align-content: center;" class="justify-content-center">
					        	<h2 style="text-align: center;" class="px-4 ">Find your next big adventure with byo</h2>
								<div style="text-align:center;" class="py-4 mb-4">
									<a href="javascript:void(0);" class="download-link px-2 mr-1 " style="display: inline-block;border-radius: 4px;align-self: center;width: 40%;"><img src="PlayStoreIcon.jpg" class="img-fluid"></a>
									<a href="javascript:void(0);" class="download-link px-2 ml-1 " style="display: inline-block;border-radius: 4px;align-self: center;width: 40%;"><img src="AppStoreIcon.jpg" class="img-fluid"></a>
								</div>
							</div>
						</div>
						<div class="col justify-content-center text-center" style="align-self: center;">
							<figure>
								<img src="https://storage.googleapis.com/byocdn/imagetranscoding/assets/images/Qr.png" class="img-fluid" style="">
								<h5>Scan the QR Code</h5>
							</figure>
						</div>
	   				</div>
				</div>
	       	</div>
		    <div class="modal-content d-lg-none">
		       	<div class="modal-body px-5">
			        <div style="text-align:center;" class="my-5">
			        	<img src="logo.svg" class="img-fluid">
			        </div>
			        <h4 style="text-align: center;" class="px-4 my-5">Find your next big adventure with byo</h4>
					<div style="text-align:center;" class="py-3 mb-5">
						<a href="javascript:void(0);" class="download-link btn btn-primary px-2" style="display: inline-block;border-radius: 4px;background-color: #37a3ff;border-color: #37a3ff;align-self: center;">Download App</a>
					</div>
		      	</div>
		    </div>
	  	</div>
	</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="owlcarousel/js/owl.carousel.min.js"></script>
    <script type="text/javascript">
    	let topbarHeight = parseFloat($('#topBar').css('height'))+10;
		$('#topSection').css('margin-top',topbarHeight+'px');

    	$(document).ready(function(){
    		let topbarHeight = parseFloat($('#topBar').css('height'))+10;
			$('#topSection').css('margin-top',topbarHeight+'px');


			$(".owl-carousel").owlCarousel({
			    loop: false,
			    // touchDrag: false,
			    smartSpeed: 1500,
			    animateIn: 'linear',
			    animateOut: 'linear',
			    responsive : {
				    
				    0 : {
				    	nav: false,
				        items: 2
				    },
				    1000 : {
				        items: 3,
				        nav: true
				    }
				}
			});

			
			$('.profile-image').each(function(){
			    if(parseInt($(this).width())>parseInt($(this).height())){
			        $(this).css('height', $(this).css('width'));
			    }else{
			        $(this).css('width', $(this).css('height'));
			    }
			});



			$.ajax({
				method: "POST",
				url: "api.php",
				data: { rid: "<?=$rid;?>", method: "inviteLink"}
			}).done(function( msg ) {
				console.log(msg);
				if(msg != ''){
					$('.download-link').attr('href', msg);
				}
			});
    	});

    	$('.open-popup').click(function(){
    		$('#myModal').modal('show');
    	});


    	$(document).on('click', '.download-link', function(e){
    		e.stopPropagation();
    		e.preventDefault();
    		if($(this).attr('href') == 'javascript:void(0);'){
    			<?php
    			if($rid > 0){
    			}else{
    				echo "window.open('https://travelwithbyo.com/byoexperience/','_blank');";
    			}
    			?>
    		}else{
    			window.open($(this).attr('href'),'_blank');
    		}
    	});

    	$(document).on('click', '.suggested-card', function(){
    		window.location.href = '?'+$(this).data('handle')<?=($rid ? "+'&ref_id=$rid'" : "");?>;
    	});

    	$(document).on('click', '.owl-next,.owl-prev', function(e){
    		var scrollLeft = parseInt($('.owl-stage').scrollLeft());
    		var itemWidth = parseInt($('.owl-item:first-child').width());
    		if($(this).hasClass('owl-next')){

	    		if(scrollLeft%itemWidth !== 0){
	    			$('.owl-stage').animate({
			            scrollLeft: scrollLeft+(itemWidth - (scrollLeft%itemWidth))
			        }, 200);
	    		}else{
	    			$('.owl-stage').animate({
			            scrollLeft: scrollLeft+itemWidth
			        }, 200);
	    		}
    		}else if($(this).hasClass('owl-prev')){
	    		if(scrollLeft%itemWidth !== 0){
	    			$('.owl-stage').animate({
			            scrollLeft: scrollLeft-(itemWidth + (scrollLeft%itemWidth))
			        }, 200);
	    		}else{
	    			$('.owl-stage').animate({
			            scrollLeft: scrollLeft-itemWidth
			        }, 200);
	    		}
    		}
    	});

    	$(document).on('click', '.grid-item', function(){
    		if($(this).data('isblog') == "1"){
    			window.location.href = '?<?=$account_handle;?>&jid='+$(this).data('jid')<?=($rid ? "+'&ref_id=$rid'" : "");?>;
    		}else{
    			$('#myModal').modal('show');
    		}
    	});

      	var IsInViewport = function(el) {
          if (typeof jQuery === "function" && el instanceof jQuery) el = el[0];
          var rect = el.getBoundingClientRect();
          return (
              rect.top >= 0 &&
              rect.left >= 0 &&
              rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
              rect.right <= (window.innerWidth || document.documentElement.clientWidth)
          );
      	};

      	$(window).scroll(function(){
      		/*$('#topBar').hide();
		    clearTimeout($.data(this, 'scrollTimer'));
		    $.data(this, 'scrollTimer', setTimeout(function() {
		        $('#topBar').show(50);
		        // console.log("Haven't scrolled in 250ms!");
		    }, 250));*/

		   if ($(window).scrollTop() >= $(document).height() - $(window).height() - 200){
		   		if($('body').data('loaded') == 1){
		   			$('#spinner').hide();
		   			$('body').data('loaded', "0");
					$('#spinner').show();
					let newOffset = parseInt($('body').data('offset'));
					$.ajax({
						method: "POST",
						url: "api.php",
						data: { memberid: "<?=$mid;?>", method: "grid", offset: newOffset, viewport: visualViewport.width}
					}).done(function( msg ) {

						if(msg != ''){
							$('#spinner').hide();
							$('body').data('offset', parseInt($('body').data('offset'))+10);
							$('.grid-item').last().after(msg);
							setTimeout(function(){
								$('body').data('loaded', "1");
								$( ".grid img,.grid video" ).each(function() {
									let newHeight = parseFloat($(this).css('width'))*1.38;
									if($(this).parent().hasClass('l1')){
										newHeight = newHeight+15;
									}
									$(".l1").css('height',newHeight+'px');
								});

								/*let l1Height = 0;

								$( ".l1" ).each(function() {
									let l1NewHeight = parseFloat($(this).css('height'));
									if(l1NewHeight >= l1Height){
										l1Height = l1NewHeight;
									}
								});
								$(".l1").css('height',l1Height+'px');*/
							},500);
						}else{
							$('#spinner').remove();
							$('#suggestedRow').parent().show();
						}
					});
				}
		   }
        	$('video').each(function(){
	            if (IsInViewport($(this))) {
	                var videos = document.querySelectorAll('video');
	                for (var i = 0; i < videos.length; i++) {
	                    videos[i].pause();
	                }
	                $(this)[0].play();
	            } else {
	                $(this)[0].pause();
	            }
        	});
      	});
	    <?php
	    if(!empty($arr)){
	    ?>

      	$.ajax({
			method: "POST",
			url: "api.php",
			data: { memberid: "<?=$mid;?>", method: "interests"}
		}).done(function( msg ) {
			$('#interestsRow').html(msg);

			$.ajax({
				method: "POST",
				url: "api.php",
				data: { memberid: "<?=$mid;?>", method: "grid", offset: $('body').data('offset'), viewport: visualViewport.width}
			}).done(function( msg ) {
				$('#spinner').hide();
				$('body').data('offset', parseInt($('body').data('offset'))+10);
				$('#topSection').after(msg);
				setTimeout(function(){
					$('body').data('loaded', "1");
					$( ".grid img,.grid video" ).each(function() {
						let newHeight = parseFloat($(this).css('width'))*1.38;
						if($(this).parent().hasClass('l1')){
							newHeight = newHeight+15;
						}
						$('.l1').css('height',newHeight+'px');
					});



					/*let l1Height = 0;

					$( ".l1" ).each(function() {
						let l1NewHeight = parseFloat($(this).css('height'));
						if(l1NewHeight >= l1Height){
							l1Height = l1NewHeight;
						}
					});
					$(".l1").css('height',l1Height+'px');*/
				},500);
			});
		});

		<?php
		}
		?>
    </script>
  </body>
</html>